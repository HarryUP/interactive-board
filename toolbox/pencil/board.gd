extends Node2D

var pool_array: Array
var color_array: Array
var size_array: Array

var pool_current: PoolVector2Array
var color_current := Color.black
var size_current := 2
var board_offset := Vector2.ZERO
var is_enabled := false
var is_draw := false

func _ready() -> void:
	for node in get_tree().get_nodes_in_group("pencil_buttons"):
		node.connect("pressed", self, "_set_pencil", [node])

func _draw() -> void:
	if is_enabled and is_draw:
		pool_current.append((get_global_mouse_position()-board_offset)/scale)
		pool_array.append(pool_current)
		color_array.append(color_current)
		size_array.append(size_current)

	for x in range(pool_array.size()):
		draw_polyline(pool_array[x], color_array[x], size_array[x], true)

func _process(delta: float) -> void:
	update()

func _set_pencil(node):
	if node.type == "color":
		color_current = node.value
	if node.type == "size":
		size_current = node.value

func _clear_drawing():
	pool_array.clear()
	color_array.clear()
	size_array.clear()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("click") and is_enabled:
		is_draw = true
	if event.is_action_released("click"):
		is_draw = false
		pool_current.resize(0)
