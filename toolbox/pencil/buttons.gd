extends Node2D

var is_enabled := false
var is_mouse_entered := false
var button_id := Vector2.ONE

func _ready() -> void:
	for node in $buttons.get_children():
		node.connect("pressed", self, "_on_button_pressed", [node])
		node.connect("mouse_entered", self, "_buttons_entered", [true])
		node.connect("mouse_exited", self, "_buttons_entered", [false])

func _process(delta: float) -> void:
	if is_enabled:
		var arrow := preload("res://assets/icon/pencil_f.png")
		$button_main.texture_normal = arrow
	else:
		var arrow := preload("res://assets/icon/pencil_n.png")
		$button_main.texture_normal = arrow
		$buttons.hide()

	for node in $buttons.get_children():
		var arrow_focused := load("res://assets/pencil/"+node.name+"_f.png")
		var arrow_normal := load("res://assets/pencil/"+node.name+"_n.png")

		if node.type == "color":
			if node.id == button_id.x:
				node.texture_normal = arrow_focused
			else:
				node.texture_normal = arrow_normal

		if node.type == "size":
			if node.id == button_id.y:
				node.texture_normal = arrow_focused
			else:
				node.texture_normal = arrow_normal

func _input(event: InputEvent) -> void:
	if is_enabled and !is_mouse_entered:
		if event.is_action_released("click"):
			$buttons.hide()

func _buttons_entered(state) -> void:
	if is_enabled:
		is_mouse_entered = state

func _on_button_main_mouse_entered() -> void:
	if is_enabled:
		is_mouse_entered = true

func _on_button_main_mouse_exited() -> void:
	if is_enabled:
		is_mouse_entered = false

func _on_button_main_pressed() -> void:
	get_owner().get_node("board").is_enabled = true
	is_enabled = true
	is_mouse_entered = true
	$buttons.show()

func _on_button_pressed(node):
	if node.type == "color":
		button_id.x = node.id
	if node.type == "size":
		button_id.y = node.id

	$buttons.hide()
	is_mouse_entered = false
