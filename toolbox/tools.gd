extends Control

var pan_enabled := false
var drag_enabled := false
var pos_offset := Vector2.ZERO
var cursor_enabled := true
var _camera_2d
var _game
var _board

func _reset_enabled():
	$zoom.is_enabled = false
	$arrow.is_enabled = false
	$pencil.is_enabled = false
	_board.is_enabled = false
	pan_enabled = false
	drag_enabled = false
	cursor_enabled = false
	Input.set_custom_mouse_cursor(null,CURSOR_ARROW)

func _ready() -> void:
	$zoom.connect("zoom_viewport_pressed", self, "_on_zoom_viewport_pressed")
	$zoom/zoom_in.connect("pressed", self, "_on_zoom_in_pressed")
	$zoom.connect("zoom_out_pressed", self, "_on_zoom_out_pressed")
	$arrow/button_main.connect("pressed", self, "_on_arrow_pressed")
	$pencil/button_main.connect("pressed", self, "_on_pencil_pressed")
	$cover/button_main.connect("pressed", self, "_on_cover_pressed")
	_camera_2d = get_parent()
	_game = get_owner().get_node("game")
	_board = get_owner().get_node("board")
	_reset_enabled()
	cursor_enabled = true
	$pointer.grab_focus()

func _process(delta: float) -> void:
	update()
	if pan_enabled and drag_enabled:
		_game.position = get_global_mouse_position() - pos_offset
		_board.position = get_global_mouse_position() - pos_offset
	_board.board_offset = _board.position
	$cover.camera_off = _camera_2d.position-(get_viewport().get_visible_rect().size/2)

func _on_zoom_viewport_pressed(z_lvl) -> void:
	_reset_enabled()
	$zoom.is_enabled = true
	var arrow = preload("res://assets/cursor/zoom_in.png")
	Input.set_custom_mouse_cursor(arrow,CURSOR_ARROW)
	_camera_2d.position = get_global_mouse_position()+(get_global_mouse_position()*1/z_lvl)

func _on_zoom_in_pressed() -> void:
	_reset_enabled()
	$zoom.is_enabled = true
	var arrow = preload("res://assets/cursor/zoom_in.png")
	Input.set_custom_mouse_cursor(arrow,CURSOR_ARROW)

func _on_zoom_out_pressed(z_lvl) -> void:
	_reset_enabled()
	_camera_2d.position -= _camera_2d.position*1/(z_lvl+1)
	if z_lvl <= 1:
		_camera_2d.position = get_viewport().get_visible_rect().size/2
		_game.position = Vector2.ZERO
		_board.position = Vector2.ZERO
		_reset_enabled()
		$pointer.grab_click_focus()
		$pointer.grab_focus()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("click") and pan_enabled:
		drag_enabled = true
		pos_offset = get_global_mouse_position() - _board.position
	if event.is_action_released("click") and pan_enabled:
		drag_enabled = false

func _on_eraser_pressed() -> void:
	_reset_enabled()
	for del in get_tree().get_nodes_in_group("deletable"):
		del.queue_free()
	_board._clear_drawing()
	$cover._clear_cover()
	cursor_enabled = true
	$pointer.grab_click_focus()
	$pointer.grab_focus()

func _on_pencil_pressed() -> void:
	_reset_enabled()
	var arrow = preload("res://assets/cursor/pencil.png")
	Input.set_custom_mouse_cursor(arrow,CURSOR_ARROW,Vector2(0,24))

func _on_arrow_pressed() -> void:
	_reset_enabled()

func _on_cover_pressed() -> void:
	_reset_enabled()

func _on_hand_pressed() -> void:
	_reset_enabled()
	if $zoom.zoom_level > 0:
		pan_enabled = true
		var arrow = preload("res://assets/cursor/grab.png")
		Input.set_custom_mouse_cursor(arrow,CURSOR_ARROW)

func _on_pointer_pressed() -> void:
	_reset_enabled()
	cursor_enabled = true

func _on_ctrl_pressed() -> void:
	_reset_enabled()
	cursor_enabled = true
	$pointer.grab_click_focus()
	$pointer.grab_focus()
