extends Node2D

onready var p := get_parent()

func _ready() -> void:
	p.get_node("pointer").connect("mouse_entered", self, "_tooltip",["Žymeklis"])
	p.get_node("pointer").connect("mouse_exited", self, "_tooltip")

	p.get_node("hand").connect("mouse_entered", self, "_tooltip",["Ranka"])
	p.get_node("hand").connect("mouse_exited", self, "_tooltip")

	p.get_node("eraser").connect("mouse_entered", self, "_tooltip",["Šalinti"])
	p.get_node("eraser").connect("mouse_exited", self, "_tooltip")

	p.get_node("arrow/button_main").connect("mouse_entered", self, "_tooltip",["Rodyklė"])
	p.get_node("arrow/button_main").connect("mouse_exited", self, "_tooltip")

	p.get_node("cover/button_main").connect("mouse_entered", self, "_tooltip",["Uždengimas"])
	p.get_node("cover/button_main").connect("mouse_exited", self, "_tooltip")

	p.get_node("zoom/zoom_in").connect("mouse_entered", self, "_tooltip",["Didinti"])
	p.get_node("zoom/zoom_in").connect("mouse_exited", self, "_tooltip")

	p.get_node("zoom/zoom_out").connect("mouse_entered", self, "_tooltip",["Mažinti"])
	p.get_node("zoom/zoom_out").connect("mouse_exited", self, "_tooltip")

	p.get_node("pencil/button_main").connect("mouse_entered", self, "_tooltip",["Pieštukas"])
	p.get_node("pencil/button_main").connect("mouse_exited", self, "_tooltip")

	for node in get_tree().get_nodes_in_group("pencil_buttons"):
		if node.type == "color":
			node.connect("mouse_entered", self, "_tooltip",["Spalva"])
		if node.type == "size":
			node.connect("mouse_entered", self, "_tooltip",["Storis"])

		node.connect("mouse_exited", self, "_tooltip")
		node.connect("pressed", self, "_tooltip")

func _on_timer_timeout() -> void:
	show()
	global_position = get_global_mouse_position()

func _tooltip(txt="") -> void:
	if txt.length() > 0:
		$label.text = txt
		$timer.stop()
		$timer.start()
	else:
		hide()
		$timer.stop()
