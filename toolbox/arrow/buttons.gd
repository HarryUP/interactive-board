extends Node2D

var node2spawn := preload("res://toolbox/arrow/sticker.tscn")
var is_enabled := false
var is_arrow := false
var is_mouse_entered := false
var button_id := 1
var button_rot := 0
var cursor_icon: Sprite

func _ready() -> void:
	for node in get_tree().get_nodes_in_group("arrows"):
		node.connect("pressed", self, "_on_button_pressed", [node])
		node.connect("mouse_entered", self, "_on_button_mouse_entered")
		node.connect("mouse_exited", self, "_on_button_mouse_exited")
	cursor_icon = node2spawn.instance()
	cursor_icon.hide()
	add_child(cursor_icon)

func _process(delta: float) -> void:
	if is_enabled:
		var arrow := preload("res://assets/icon/arrow_f.png")
		$button_main.texture_normal = arrow
	else:
		var arrow := preload("res://assets/icon/arrow_n.png")
		$button_main.texture_normal = arrow
		$buttons.hide()

	for node in get_tree().get_nodes_in_group("arrows"):
		match node.id:
			1,2,5,6:
				var arrow_focused := preload("res://assets/icon/arrow_up_f.png")
				var arrow_normal := preload("res://assets/icon/arrow_up.png")
				if node.id == button_id:
					node.texture_normal = arrow_focused
				else:
					node.texture_normal = arrow_normal
			3,4,7,8:
				var arrow_focused := preload("res://assets/icon/arrow_upright_f.png")
				var arrow_normal := preload("res://assets/icon/arrow_upright.png")
				if node.id == button_id:
					node.texture_normal = arrow_focused
				else:
					node.texture_normal = arrow_normal

func _input(event: InputEvent) -> void:
	if is_enabled and !is_mouse_entered and is_arrow:
		cursor_icon.show()
		cursor_icon.global_position = get_global_mouse_position()
		cursor_icon.rotation_degrees = button_rot
		if event.is_action_released("click"):
			$buttons.hide()
	else:
		cursor_icon.hide()

	if is_enabled and event.is_action_released("click") and is_arrow and !is_mouse_entered:
		var node_instance = node2spawn.instance()
		get_owner().get_node("board").add_child(node_instance)
		node_instance.add_to_group("deletable")
		node_instance.global_position = get_global_mouse_position()
		node_instance.rotation_degrees = button_rot

func _on_button_mouse_entered() -> void:
	if is_enabled:
		is_mouse_entered = true
		is_arrow = false

func _on_button_mouse_exited() -> void:
	if is_enabled:
		is_mouse_entered = false
		is_arrow = true

func _on_button_main_mouse_entered() -> void:
	if is_enabled:
		is_mouse_entered = true

func _on_button_main_mouse_exited() -> void:
	if is_enabled:
		is_mouse_entered = false
		is_arrow = true

func _on_button_main_pressed() -> void:
	is_enabled = true
	is_mouse_entered = true
	$buttons.show()

func _on_button_pressed(node) -> void:
	button_id = node.id
	button_rot = node.rot
	is_arrow = true
	$buttons.hide()
	is_mouse_entered = false
