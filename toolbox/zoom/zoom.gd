extends Node2D

signal zoom_viewport_pressed
signal zoom_in_pressed
signal zoom_out_pressed
var is_enabled := false
var is_zoom := false
var is_mouse_entered := false

var zoom_factor := Vector2(.5,.5)
var zoom_level := 1
var zoom_max := 4
var zoom_mode := false

func _ready() -> void:
	if zoom_level <= 1:
		$zoom_out.disabled = true

func _input(event: InputEvent) -> void:
	if is_enabled:
		if event.is_action_released("click") and is_zoom and zoom_mode and !is_mouse_entered:
			if zoom_level >= 1 and zoom_level < zoom_max:
				zoom_level += 1
				get_owner().get_node("game").scale += zoom_factor
				get_owner().get_node("board").scale += zoom_factor
				$zoom_out.disabled = false
				emit_signal("zoom_viewport_pressed",zoom_level)

func _on_zoom_in_pressed() -> void:
	emit_signal("zoom_in_pressed",zoom_level)
	is_enabled = true
	is_zoom = false
	zoom_mode = true

func _on_zoom_in_mouse_entered() -> void:
	if is_enabled:
		is_zoom = false
		is_mouse_entered = true

func _on_zoom_in_mouse_exited() -> void:
	if is_enabled:
		is_zoom = true
		is_mouse_entered = false

func _on_zoom_out_pressed() -> void:
	is_enabled = true
	is_zoom = true
	zoom_mode = false

	if is_enabled and !zoom_mode:
		if zoom_level > 1:
			zoom_level -= 1
			get_owner().get_node("game").scale -= zoom_factor
			get_owner().get_node("board").scale -= zoom_factor
			emit_signal("zoom_out_pressed",zoom_level)
		if zoom_level <= 1:
			$zoom_out.disabled = true
			is_enabled = false

func _on_zoom_out_mouse_entered() -> void:
	if is_enabled:
		is_mouse_entered = true

func _on_zoom_out_mouse_exited() -> void:
	if is_enabled:
		is_mouse_entered = false
