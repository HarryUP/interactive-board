extends Node2D

var anchor
onready var rect2draw = Rect2(Vector2.ZERO,Vector2.ONE)
var is_enabled := false
var is_block := false
var is_moving := false
var is_zoom := false
var camera_off := Vector2.ZERO
var a_array := []
var b_array := []
var c_array := []
const x_but_margin = Vector2(-25,10)

func _ready() -> void:
	$anchors/x_button.rect_global_position = $anchors/tr.global_position + x_but_margin
	$anchors.hide()
	for node in get_tree().get_nodes_in_group("anchors"):
		node.connect("resize_me", self, "_resize_me")

func _draw() -> void:
	if is_block:
		draw_rect(rect2draw,Color.white,true)
		draw_rect(rect2draw,Color.black,false,2)

func _process(delta: float) -> void:
	update()
	rect2draw = Rect2($anchors/tl.global_position-camera_off,$anchors/br.global_position-$anchors/tl.global_position)

func _input(event: InputEvent) -> void:
	if is_enabled:
		$anchors/mc/collision_shape_2d.transform = Transform2D(Vector2(rect2draw.size.x/100,0), Vector2(0,rect2draw.size.y/100), Vector2.ZERO)
		is_moving = false
		match anchor.id:
			0:
				a_array = [$anchors/tl,$anchors/ml,$anchors/bl]
				b_array = [$anchors/tr,$anchors/mr,$anchors/br]
				c_array = [$anchors/tc,$anchors/mc,$anchors/bc]
				_redimension(a_array,b_array,c_array)
			1:
				a_array = [$anchors/tr,$anchors/mr,$anchors/br]
				b_array = [$anchors/tl,$anchors/ml,$anchors/bl]
				c_array = [$anchors/tc,$anchors/mc,$anchors/bc]
				_redimension(a_array,b_array,c_array)
			2:
				a_array = [$anchors/bl,$anchors/ml,$anchors/tl]
				b_array = [$anchors/br,$anchors/mr,$anchors/tr]
				c_array = [$anchors/bc,$anchors/mc,$anchors/tc]
				_redimension(a_array,b_array,c_array)
			3:
				a_array = [$anchors/br,$anchors/mr,$anchors/tr]
				b_array = [$anchors/bl,$anchors/ml,$anchors/tl]
				c_array = [$anchors/bc,$anchors/mc,$anchors/tc]
				_redimension(a_array,b_array,c_array)
			4:
				a_array = [$anchors/tc,$anchors/mc,$anchors/bc]
				b_array = [$anchors/tr,$anchors/mr,$anchors/br]
				c_array = [$anchors/tl,$anchors/ml,$anchors/bl]
				_redimension(a_array,b_array,c_array)
			5:
				a_array = [$anchors/ml,$anchors/mc,$anchors/mr]
				b_array = [$anchors/tl,$anchors/tc,$anchors/tr]
				c_array = [$anchors/bl,$anchors/bc,$anchors/br]
				_redimension(a_array,b_array,c_array)
			6:
				a_array = [$anchors/mr,$anchors/mc,$anchors/ml]
				b_array = [$anchors/br,$anchors/bc,$anchors/bl]
				c_array = [$anchors/tr,$anchors/tc,$anchors/tl]
				_redimension(a_array,b_array,c_array)
			7:
				a_array = [$anchors/bc,$anchors/mc,$anchors/tc]
				b_array = [$anchors/bl,$anchors/ml,$anchors/tl]
				c_array = [$anchors/br,$anchors/mr,$anchors/tr]
				_redimension(a_array,b_array,c_array)
			8:
				a_array = [$anchors/bl,$anchors/ml,$anchors/tl]
				b_array = [$anchors/br,$anchors/mr,$anchors/tr]
				c_array = [$anchors/tc,$anchors/mc,$anchors/bc]
				_redimension(a_array,b_array,c_array)

func _resize_me(node):
	anchor = node
	is_enabled = node.is_enabled

#[a0]=[c0]=[b0]
#||    ||    ||
#[a1]=[c1]=[b1]
#||    ||    ||
#[a2]=[c2]=[b2]
func _redimension(a,b,c):
	match anchor.id:
		0,1,2,3:
			anchor.global_position = get_global_mouse_position()
			a[2].global_position = Vector2(a[0].global_position.x,b[2].global_position.y)
			b[0].global_position = Vector2(b[2].global_position.x,a[0].global_position.y)
			var center := Vector2(a[0].global_position.x + ((b[0].global_position.x-a[0].global_position.x)/2),a[0].global_position.y + ((a[2].global_position.y-a[0].global_position.y)/2))
			c[0].global_position = Vector2(center.x, a[0].global_position.y)
			c[1].global_position = Vector2(center.x, a[1].global_position.y)
			c[2].global_position = Vector2(center.x, a[2].global_position.y)
			a[1].global_position = Vector2(a[0].global_position.x, center.y)
			b[1].global_position = Vector2(b[0].global_position.x, center.y)

			if a[0].global_position.x > b[0].global_position.x:
				$anchors/x_button.rect_global_position = a[0].global_position + x_but_margin
			else:
				$anchors/x_button.rect_global_position = b[0].global_position + x_but_margin
			if a[0].global_position.y > a[2].global_position.y:
				$anchors/x_button.rect_global_position.y = a[2].global_position.y + x_but_margin.y
		4,7:
			a[0].global_position.y = get_global_mouse_position().y
			b[0].global_position.y = a[0].global_position.y
			c[0].global_position.y = a[0].global_position.y

			a[1].global_position.y = a[0].global_position.y + ((a[2].global_position.y-a[0].global_position.y)/2)
			b[1].global_position.y = a[1].global_position.y
			c[1].global_position.y = a[1].global_position.y

			if a[0].global_position.y > a[2].global_position.y:
				$anchors/x_button.rect_global_position.y = a[2].global_position.y + x_but_margin.y
			else:
				$anchors/x_button.rect_global_position.y = a[0].global_position.y + x_but_margin.y
		5,6:
			a[0].global_position.x = get_global_mouse_position().x
			b[0].global_position.x = a[0].global_position.x
			c[0].global_position.x = a[0].global_position.x

			a[1].global_position.x = a[0].global_position.x + ((a[2].global_position.x-a[0].global_position.x)/2)
			b[1].global_position.x = a[1].global_position.x
			c[1].global_position.x = a[1].global_position.x

			if a[0].global_position.x < a[2].global_position.x:
				if b[0].global_position.y < c[0].global_position.y:
					$anchors/x_button.rect_global_position = b[2].global_position + x_but_margin
				else:
					$anchors/x_button.rect_global_position = c[2].global_position + x_but_margin

			if a[0].global_position.x > a[2].global_position.x:
				if b[0].global_position.y < c[0].global_position.y:
					$anchors/x_button.rect_global_position = b[0].global_position + x_but_margin
				else:
					$anchors/x_button.rect_global_position = c[0].global_position + x_but_margin
		8:
			is_moving = true
			if !is_zoom:
				rect2draw = Rect2(get_global_mouse_position()-(rect2draw.size/2),rect2draw.size)
				c[1].global_position = get_global_mouse_position()
				a[2].global_position = rect2draw.position
				b[2].global_position = Vector2(rect2draw.position.x+rect2draw.size.x,rect2draw.position.y)
				a[0].global_position = Vector2(rect2draw.position.x,rect2draw.position.y+rect2draw.size.y)
				b[0].global_position = rect2draw.position+rect2draw.size
				c[0].global_position = Vector2(rect2draw.position.x+(rect2draw.size.x/2),rect2draw.position.y)
				a[1].global_position = Vector2(rect2draw.position.x,rect2draw.position.y+(rect2draw.size.y)/2)
				b[1].global_position = Vector2(rect2draw.position.x+rect2draw.size.x,rect2draw.position.y+(rect2draw.size.y)/2)
				c[2].global_position = Vector2(rect2draw.position.x+(rect2draw.size.x/2),rect2draw.position.y+rect2draw.size.y)

				$anchors/x_button.rect_global_position = Vector2(rect2draw.position.x+rect2draw.size.x,rect2draw.position.y)+ x_but_margin

			if a[0].global_position.x > b[0].global_position.x:
				$anchors/x_button.rect_global_position = a[0].global_position + x_but_margin
			else:
				$anchors/x_button.rect_global_position = b[0].global_position + x_but_margin
			if a[0].global_position.y > a[2].global_position.y:
				$anchors/x_button.rect_global_position.y = a[2].global_position.y + x_but_margin.y

func _on_Button_pressed() -> void:
	_clear_cover()

func _clear_cover() -> void:
	is_enabled = false
	$anchors.hide()
	is_block = false

func _on_button_main_pressed() -> void:
	$anchors.show()
	if !is_block:
		rect2draw = Rect2($anchors/tl.global_position,$anchors/br.global_position-$anchors/tl.global_position)
	is_block = true
