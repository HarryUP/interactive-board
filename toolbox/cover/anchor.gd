extends Node2D

export var id := 0
signal resize_me
var is_enabled := false

func _enable(s):
	is_enabled = s
	emit_signal("resize_me", self)

func _on_button_button_down() -> void:
	_enable(true)

func _on_button_button_up() -> void:
	_enable(false)

func _on_mc_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("click"):
		_enable(true)
	if event.is_action_released("click"):
		_enable(false)

func _on_mc_button_up() -> void:
	_enable(false)
func _on_mc_button_down() -> void:
	_enable(true)
